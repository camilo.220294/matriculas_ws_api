/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.matriculas.controller;

import com.test.matriculas.model.Curso;
import com.test.matriculas.model.Estudiante;
import com.test.matriculas.model.Matricula;
import com.test.matriculas.repository.CursoRepository;
import com.test.matriculas.repository.EstudianteRepository;
import com.test.matriculas.repository.MatriculaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author CAMILO
 */
@Controller
@RequestMapping(path = "/matriculas")
public class MatriculaController {

    @Autowired
    private MatriculaRepository matriculaRepository;

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private CursoRepository cursoRepository;

    @CrossOrigin(origins = "*")
    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<Matricula> getAllMatriculas() {
        return matriculaRepository.findAll();
    }

    @CrossOrigin(origins = "*")
    @GetMapping(path = "/get")
    public @ResponseBody
    Iterable<Matricula> getMatriculasByEstudiante(@RequestParam long idestudiante) {
        Estudiante estudiate = estudianteRepository.findOne(idestudiante);
        return estudiate.getMatriculas();
    }

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/delete")
    public @ResponseBody
    String deleteMatricula(@RequestParam long id) {
        matriculaRepository.delete(id);
        return "Borrado";
    }

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/update")
    public @ResponseBody
    String updateMatricula(@RequestParam long id, float calificacion) {

        Matricula matricula = matriculaRepository.findOne(id);
        matricula.setCalificacion(calificacion);
        matriculaRepository.save(matricula);
        return "Actualizado";
    }

    @CrossOrigin(origins = "*")
    @PostMapping(path = "/add")
    public @ResponseBody
    String addMatricula(@RequestParam long idcurso,
            @RequestParam long idestudiante,
            @RequestParam float calificacion) {

        Estudiante estudiante = estudianteRepository.findOne(idestudiante);

        Curso curso = cursoRepository.findOne(idcurso);

        Matricula matricula = new Matricula();
        matricula.setCalificacion(calificacion);
        matricula.setCurso(curso);
        matricula.setEstudiante(estudiante);
        matriculaRepository.save(matricula);
        return "Guardado";
    }

}
