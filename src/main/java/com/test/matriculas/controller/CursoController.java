/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.matriculas.controller;

import com.test.matriculas.model.Curso;
import com.test.matriculas.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author CAMILO
 */
@Controller
@RequestMapping(path = "/cursos")
public class CursoController {

    @Autowired
    private CursoRepository cursoRepository;

    @CrossOrigin(origins = "*")
    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<Curso> getAllCursos() {
        return cursoRepository.findAll();
    }

}
