/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.matriculas.repository;

import com.test.matriculas.model.Estudiante;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author CAMILO
 */
public interface EstudianteRepository extends CrudRepository<Estudiante, Long> {

}
