INSERT INTO estudiantes (id, nombre, identificacion) VALUES (nextval('hibernate_sequence'),'CAMILO CAMARGO', '108291');
INSERT INTO estudiantes (id, nombre, identificacion) VALUES (nextval('hibernate_sequence'),'SANTIAGO CAMARGO', '109034');

INSERT INTO cursos (id, nombre, codigo) VALUES (nextval('hibernate_sequence'),'ALGEBRA LINEAL', 'BAS001');
INSERT INTO cursos (id, nombre, codigo) VALUES (nextval('hibernate_sequence'),'ESTRUCTURA DE DATOS', 'SIS001');
INSERT INTO cursos (id, nombre, codigo) VALUES (nextval('hibernate_sequence'),'REDES', 'SIS002');
INSERT INTO cursos (id, nombre, codigo) VALUES (nextval('hibernate_sequence'),'FISICA', 'BAS002');
INSERT INTO cursos (id, nombre, codigo) VALUES (nextval('hibernate_sequence'),'COMPILADORES', 'SIS003');

drop FUNCTION get_calificaciones(int);
CREATE OR REPLACE FUNCTION get_calificaciones(_estudiante_id int)
  RETURNS TABLE(curso VARCHAR, codcurso VARCHAR, calificacion NUMERIC) AS
  $BODY$
  DECLARE

  BEGIN
    RETURN QUERY SELECT c2.codigo, c2.nombre, m2.calificacion
              FROM matriculas m2
                INNER JOIN cursos c2 ON m2.curso_id = c2.id
              WHERE m2.estudiante_id = _estudiante_id;
  END;
  $BODY$
  LANGUAGE plpgsql;
